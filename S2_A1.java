package com.zuitt;
import java.util.Scanner;

public class S2_A1 {
    public static void main(String[] args) {
        Scanner yearInput = new Scanner(System.in);
        System.out.print("Input Year to be check if a leap year: ");

        int year = yearInput.nextInt();

        boolean isLeapYear = false;

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    isLeapYear = true;
                }
            } else {
                isLeapYear = true;
            }
        }

        if (isLeapYear){
            System.out.println(year + " is a leap year");
        } else {
            System.out.println(year + " is NOT a leap year");
        }

    }
}
