package com.zuitt;

import java.util.Arrays;
import java.util.HashMap;

public class S2_A2 {
    public static void main(String[] args) {
        int[] primeNumbers =  {2, 3, 5, 7, 11};
        System.out.println("The First Prime Number is: " + primeNumbers[0]);

        String[] friends = {"John", "Jane", "Chloe", "Zoey"};
        System.out.println("My friends are: " + Arrays.toString(friends));

        HashMap<String, Integer>inventory = new HashMap();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);

    }
}
